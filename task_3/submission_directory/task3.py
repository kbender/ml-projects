import pandas as pd
import numpy as np
import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader, TensorDataset
import torch.nn.functional as F
import torch.optim as optim
from sklearn.metrics import f1_score

aminoAcids = ['R', 'H', 'K', 'D', 'E', 'S', 'T', 'N', 'Q', 'C', 'U', 'G', 'P', 'A' , 'V', 'I', 'L', 'M', 'F', 'W', 'Y']
nAmino = len(aminoAcids)

def letterToIndex(letter):
    return aminoAcids.index(letter)

def letterToArray(letter):
    arr = np.zeros(nAmino)
    arr[letterToIndex(letter)] = 1
    return arr

def transformSequence(sequence, active, test):
    if test:
        seqArray = np.zeros(len(sequence)*nAmino)
    else:
        seqArray = np.zeros(len(sequence)*nAmino + 1)

    for li, letter in enumerate(sequence):
        seqArray[ li*nAmino + letterToIndex(letter)] = 1

    if not test:
        seqArray[-1] = active
    return seqArray

def transformInput(df, test = False):
    if test:
        transformed = pd.DataFrame(columns = ['R1', 'H1', 'K1', 'D1', 'E1', 'S1', 'T1', 'N1', 'Q1', 'C1', 'U1', 'G1', 'P1', 'A1' , 'V1', 'I1', 'L1', 'M1', 'F1', 'W1', 'Y1', 'R2', 'H2', 'K2', 'D2', 'E2', 'S2', 'T2', 'N2', 'Q2', 'C2', 'U2', 'G2', 'P2', 'A2' , 'V2', 'I2', 'L2', 'M2', 'F2', 'W2', 'Y2', 'R3', 'H3', 'K3', 'D3', 'E3', 'S3', 'T3', 'N3', 'Q3', 'C3', 'U3', 'G3', 'P3', 'A3' , 'V3', 'I3', 'L3', 'M3', 'F3', 'W3', 'Y3', 'R4', 'H4', 'K4', 'D4', 'E4', 'S4', 'T4', 'N4', 'Q4', 'C4', 'U4', 'G4', 'P4', 'A4' , 'V4', 'I4', 'L4', 'M4', 'F4', 'W4', 'Y4'])
    else:
        transformed = pd.DataFrame(columns = ['R1', 'H1', 'K1', 'D1', 'E1', 'S1', 'T1', 'N1', 'Q1', 'C1', 'U1', 'G1', 'P1', 'A1' , 'V1', 'I1', 'L1', 'M1', 'F1', 'W1', 'Y1', 'R2', 'H2', 'K2', 'D2', 'E2', 'S2', 'T2', 'N2', 'Q2', 'C2', 'U2', 'G2', 'P2', 'A2' , 'V2', 'I2', 'L2', 'M2', 'F2', 'W2', 'Y2', 'R3', 'H3', 'K3', 'D3', 'E3', 'S3', 'T3', 'N3', 'Q3', 'C3', 'U3', 'G3', 'P3', 'A3' , 'V3', 'I3', 'L3', 'M3', 'F3', 'W3', 'Y3', 'R4', 'H4', 'K4', 'D4', 'E4', 'S4', 'T4', 'N4', 'Q4', 'C4', 'U4', 'G4', 'P4', 'A4' , 'V4', 'I4', 'L4', 'M4', 'F4', 'W4', 'Y4', 'Active'])
    
    if test:
        arrData = np.zeros((df.size, 84))
    else:
        arrData = np.zeros((df.size, 85))

    for  index, row in df.iterrows():

        sequence = row['Sequence']

        active = 0
        if not test:
            active = row['Active']

        arrData[index, :] = transformSequence(sequence, active, test)

        if index % 10000 == 0:
            print(f'finished processing index  %.d' % index, end='\r')


    data = pd.DataFrame(arrData, columns = transformed.columns)
    transformed = transformed.append(data, ignore_index=True)

    return transformed



def score_submission(predictions, labels):
    return f1_score(labels,predictions)