import torch as torch
from torch.utils.data import DataLoader


device = torch.device("cpu")

relu_activation = torch.nn.ReLU()


class Net(torch.nn.Module):



    def __init__(self):

        super(Net, self).__init__()
        self.hid1 = torch.nn.Linear(84, 40, bias=True)# 4-(8-8)-1
        self.hid2 = torch.nn.Linear(40, 20, bias=True)
        self.hid3 = torch.nn.Linear(20, 10, bias=True)
        self.hid4 = torch.nn.Linear(10, 5, bias=True)
        self.oupt = torch.nn.Linear(5, 1, bias=True)

        torch.nn.init.xavier_uniform_(self.hid1.weight)
        torch.nn.init.zeros_(self.hid1.bias)
        torch.nn.init.xavier_uniform_(self.hid2.weight)
        torch.nn.init.zeros_(self.hid2.bias)
        torch.nn.init.xavier_uniform_(self.hid3.weight)
        torch.nn.init.zeros_(self.hid3.bias)
        torch.nn.init.xavier_uniform_(self.hid4.weight)
        torch.nn.init.zeros_(self.hid4.bias)
        torch.nn.init.xavier_uniform_(self.oupt.weight)
        torch.nn.init.zeros_(self.oupt.bias)

    def forward(self, x):
        drop = torch.nn.Dropout(p=0.25)

        z = relu_activation(self.hid1(x))
        z = relu_activation(self.hid2(drop(z)))
        z = relu_activation(self.hid3(drop(z)))
        z = relu_activation(self.hid4(drop(z)))
        z = self.oupt(z)

        return z

