
### INITIALIZE ###

#load relevant libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd #for reading csv file
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import Lasso, LassoCV
from sklearn.model_selection import cross_validate, KFold, train_test_split
from sklearn.pipeline import Pipeline
import features
import matplotlib.pyplot as plt
from threading import Thread

# load data
train_data = pd.read_csv('train.csv', sep=',').values
y_input = train_data[:, 1]
x_input = train_data[:,2:]
phi = np.apply_along_axis(features.feature_vector,1,x_input)

norms = [np.linalg.norm(phi[i,:], ord=2) for i in range(0,len(y_input))]
print(np.average(norms))



# we need our custom scorer
def root_mean_squared_error(estimator,X,y_true):
    y_pred = estimator.predict(X)
    return mean_squared_error(y_true,y_pred)**0.5

model_parameters = {'fit_intercept':False, 'normalize': False}
splitter = KFold(shuffle=True, random_state=5, n_splits=10)

def lasso_and_average_score(penalty, idx, scores_for_pens):
    model = Lasso(alpha=penalty, **model_parameters, max_iter=10000)
    result = cross_validate(model, phi, y_input, cv=splitter, scoring=root_mean_squared_error,)
    scores = result['test_score']
    avg_score = np.average(scores)
    scores_for_pens[idx] = (avg_score)


def cross_validate_penalties():

    lasso_penalty = np.linspace(0.01,0.5,100)

    scores_for_penalties = np.zeros(len(lasso_penalty))

    threads = [None]*len(lasso_penalty)

    for index, penalty in enumerate(lasso_penalty):
        threads[index] = Thread(target=lasso_and_average_score, args=[penalty, index,scores_for_penalties])
        threads[index].start()

    for thread in threads:
        thread.join()

    best_penalty = np.argmin(scores_for_penalties)
    best_penalty = lasso_penalty[best_penalty]
    print(best_penalty)

    model = Lasso(alpha=best_penalty,**model_parameters)
    model.fit(phi, y_input)
    return model.coef_


def gradient_descent_penalty():
    alphas = np.linspace(0.01,0.5,1000)
    model = LassoCV(normalize=False, fit_intercept=False , max_iter=10000, eps=1e-2, alphas=alphas, cv=10,
                    random_state=5)
    model.fit(phi, y_input)
    print("alpha automatic: %.5f" %model.alpha_)

    return model.coef_


def evaluate_and_print(coeffs, name):
    predictions_auto = np.matmul(phi, coeffs)
    errors_auto = predictions_auto - y_input
    RMSE_auto = np.sqrt(1 / len(y_input) * np.linalg.norm(errors_auto, ord=2) ** 2)
    print(f'RMSE {name}: %.4f' % RMSE_auto)


coeffs = gradient_descent_penalty()
#coeffs_handmade = cross_validate_penalties()

### THIS IS WHAT THE TEACHING ASSISTANTS WILL DO SO LETS CHECK
evaluate_and_print(coeffs, 'auto')
#evaluate_and_print(coeffs_handmade, 'hand')


