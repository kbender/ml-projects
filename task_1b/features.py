import numpy as np

def linear(x):
    return x

def quadratic(x):
    return x**2

def exponential(x):
    return np.exp(x)

def cosine(x):
    return np.cos(x)


def feature_vector(x):
    result = np.zeros(21)
    result[0:5] = linear(x)
    result[5:10] = quadratic(x)
    result[10:15] = exponential(x)
    result[15:20] = cosine(x)
    result[20] = 1
    return result

