import torch
from torch import Tensor
from torch.utils.data import Dataset
import pandas as pd
import os
import numpy as np
from PIL import Image
from threading import Thread
from torchvision import transforms
from torch.utils.data import DataLoader, TensorDataset
import helpers


# I used this website https://pytorch.org/tutorials/beginner/data_loading_tutorial.html

N_OF_IMAGES = 10000 # this is the total number of images in the files, do not change.

# this function is called for every thread, it gets some indexes and puts the processed images in the tensor

def load_and_transform(path, transform = None):

    image = Image.open(path)
    if transform is not None:
        image_t = transform(image)
        del image
    else:
        image_t = image

    return image_t


class TripletDataset(Dataset):

    def __new__(self, txt_file, root_dir, transform=None, size=224, n_of_samples=None,
                drop_duplicates = True, *args, **kwargs):
        print("Creating Instance")
        instance = super(TripletDataset, self).__new__(self, *args, **kwargs)
        return instance

    def __init__(self, txt_file, root_dir, transform=None, size=224, n_of_samples=None,
                    drop_duplicates = True, *args, **kwargs):
        """

        :param txt_file: the complete path to the triplets text file
        :param root_dir: the directoy where the .jpg files are saved
        :param transform: the transformation to apply. If none, the images will just be placed in the tensor
        :param size: the number of pixels on each side (images are squared as of right now)
        :param n_of_samples: the number of triplets to process. Set to none for full dataset.
        """
        global N_OF_THREADS

        super().__init__(*args, **kwargs)

        self.root_dir = root_dir

        self.N_OF_IMAGES = N_OF_IMAGES # this is the total number of images in the files, do not change.

        self.transform = transform

        self.triplets_frame = pd.read_table(txt_file, header=None, sep=' ')
        if drop_duplicates:
            self.triplets_frame = self.triplets_frame.drop_duplicates()

        # if reduced dataset, we have true here and change some things.
        if n_of_samples is not None:
            # reduce number of triplets in dataframe
            self.triplets_frame = self.triplets_frame.iloc[:n_of_samples]



    def embed(self, net, batch_size, device, n_features):

        dataset = torch.utils.data.TensorDataset(self.tensor)
        loader = DataLoader(dataset, batch_size, shuffle=False)

        embeddings = np.zeros((len(dataset), n_features))
        net.to(device)
        net.eval()

        with torch.no_grad():
            for idx, data in enumerate(loader):

                image_tensor = data[0]
                image_tensor = image_tensor.to(device)

                vectors = net(image_tensor)
                vectors = vectors.cpu().numpy()
                embeddings[idx*batch_size:idx*batch_size + batch_size] = vectors

        self.embeddings = torch.from_numpy(embeddings).float()


    def __len__(self):

        return len(self.triplets_frame)



    def __getitem__(self, idx):

        if torch.is_tensor(idx):
            idx = idx.tolist()

        anchor_name = self.triplets_frame.iloc[idx, 0]
        pos_name = self.triplets_frame.iloc[idx, 1]
        neg_name = self.triplets_frame.iloc[idx, 2]

        anchor_path = os.path.join(self.root_dir,
                                   str(anchor_name).zfill(5) + '.jpg')

        pos_path = os.path.join(self.root_dir,
                                   str(pos_name).zfill(5) + '.jpg')

        neg_path = os.path.join(self.root_dir,
                                str(neg_name).zfill(5) + '.jpg')

        paths = [anchor_path, pos_path, neg_path]

        sample = [load_and_transform(path, self.transform) for path in paths]

        return sample


def make_holdout_text(txt_file, target_dir, percentage = 0.3, unique_string = None):
    triplets_frame = pd.read_table(txt_file, header=None, sep=' ').values
    holdout_images = np.random.randint(0,N_OF_IMAGES,int(N_OF_IMAGES*percentage))
    mask = (np.isin(triplets_frame, holdout_images).sum(axis=1) > 0)
    holdout_triplets = pd.DataFrame(triplets_frame[mask])
    triplets_frame = pd.DataFrame(triplets_frame[mask == 0])

    if unique_string is None:
        unique_string = helpers.unique_string()

    holdout_triplets.to_csv(target_dir + 'HOLD_' + str(int(percentage*100)) +  '_'  + unique_string + '.txt', sep =' ', index=False, header=False)
    triplets_frame.to_csv(target_dir + 'TRAIN_' + str(int(percentage * 100)) + '_'  + unique_string + '.txt', sep=' ', index=False, header=False)


class ImageDataset(Dataset):
    def __init__(self, img_dir, transform=None):
        self.img_dir = img_dir
        self.transform = transform
        self.img_frame = np.arange(10000)
        super().__init__()
        
    def __len__(self):
        return len(self.img_frame)
    
    def __getitem__(self,idx):
        img_path = os.path.join(self.img_dir, str(idx).zfill(5) + '.jpg')
        image = load_and_transform(img_path, self.transform)
        return idx, image
        
def get_pos_and_negative_samples(txt_file, target_dir):

    triplets_frame = pd.read_table(txt_file, header=None, sep=' ').values
    anch = triplets_frame[:,0]
    pos = triplets_frame[:, 1]
    neg = triplets_frame[:, 2]
    # we want to double in size, so get two boolean arrays of len=triplets
    false = np.zeros(triplets_frame.shape[0], dtype=bool)
    true = np.ones(triplets_frame.shape[0], dtype=bool)

    # this is the label / ground truth
    result_truth = np.concatenate((false,true))
    np.random.shuffle(result_truth)

    # lets make tuples
    pos_doubles = np.stack((anch,pos))
    neg_doubles = np.stack((anch, neg))

    # allocate memory for randomly arranged tuples
    result_doubles = np.empty((2, 2*triplets_frame.shape[0]))

    # if the label is true, fill in a true double!
    result_doubles[:,result_truth] = pos_doubles
    # if the label is false, fill in a negative double!
    result_doubles[:,result_truth==0] = neg_doubles

    doubles = pd.DataFrame(result_doubles.astype(int).T)
    labels = pd.DataFrame(result_truth.astype(int))


    doubles.to_csv(target_dir + 'train_tuples.txt', sep=' ', line_terminator='\n',
                          index=False, header=False)

    labels.to_csv(target_dir + 'train_labels.txt', sep=' ',
                          index=False, header=False)

    return result_doubles.astype(int).T, result_truth.astype(int)


def get_pos_and_negative_test(txt_file):

    triplets_frame = pd.read_table(txt_file, header=None, sep=' ')
    triplets_frame = triplets_frame.values


    # this is the label / ground truth
    truth_tupel = (True,False) # only for your reference: 1 -> this is (A,B). 0 -> this is (A,C)
    truth_list = [truth_tupel] * triplets_frame.shape[0]
    truth_vector = np.asarray(truth_list).flatten() # truth vector

    # lets make tuples
    pos_doubles = triplets_frame[:,[0,1]] # (A,B)
    neg_doubles = triplets_frame[:,[0,2]] # (A,C)

    # allocate memory for randomly arranged tuples
    result_tuples = np.ones((2*triplets_frame.shape[0],2)).astype(int)

    # if the label is true, fill in a true double!
    result_tuples[truth_vector,:] = pos_doubles # here we fill in (A,B)
    # if the label is false, fill in a negative double!
    result_falses = np.invert(truth_vector)
    result_tuples[result_falses,:] = neg_doubles # (here we will in (A,C)


    return result_tuples.astype(int), truth_vector # also return truth vector, not necessary