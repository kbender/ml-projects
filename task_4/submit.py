
import preprocessing
import torch
import numpy as np

def make_decision(ab,ac):
    if (ab < ac):
        return 1  # i.e. anchor is closer to positive then negative
    else:
        return 0


make_batch_decision = np.vectorize(make_decision)


def compute_submission(model, test_dataset, test_batch_size, gitlab, device):

    # Prepare the predicitons
    predictions = np.zeros(len(test_dataset))

    # Prepare dataloader to compute the prediciton

    if (gitlab):
        testloader = torch.utils.data.DataLoader(test_dataset, batch_size=test_batch_size, shuffle=False)
    if (not gitlab):
        testloader = torch.utils.data.DataLoader(test_dataset, batch_size=test_batch_size, shuffle=False,
                                                 pin_memory=True, num_workers=2)

    with torch.no_grad():
        for i, data in enumerate(testloader):
            anchors, positives, negatives = data
            anchors = anchors.to(device)
            positives = positives.to(device)
            negatives = negatives.to(device)
            a = model(anchors).cpu().numpy()
            b = model(positives).cpu().numpy()
            c = model(negatives).cpu().numpy()

            ab = np.linalg.norm(a-b, axis=1)
            ac = np.linalg.norm(a-c, axis=1)

            labels = make_batch_decision(ab,ac)
            # print(labels)
            predictions[i*len(ab):i*len(ab)+ len(ab)] = labels

    del testloader

    return predictions