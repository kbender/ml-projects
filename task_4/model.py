import torch
from torch import nn
import torch.nn.functional as F
import helpers



class EmbeddingNetwork(torch.nn.Module):

    def __init__(self, w_in, h_in):
        super(EmbeddingNetwork,self).__init__()
        h, w = helpers.conv2d_output_dims(w_in, h_in, padding=0, kernel_size=5, stride=1)
        # c_in, c_out, kernel_size
        self.conv1 = nn.Conv2d(3,6,5)
        self.pool = nn.MaxPool2d(2, 2)
        h, w = helpers.conv2d_output_dims(w, h, padding=0, kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        h, w = helpers.conv2d_output_dims(w, h, padding=0, kernel_size=5, stride=1)
        # second pooling
        h, w = helpers.conv2d_output_dims(w, h, padding=0, kernel_size=2, stride=2)
        self.fc1 = nn.Linear(16 * w * h, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


class ClassificationModule(torch.nn.Module):

    # class variables to avoid hardcoding

    def __init__(self, n_outputs, n_inputs=1000, p_dropout=0.5):
        super(ClassificationModule, self).__init__()
        self.f1 = nn.Linear(n_inputs, n_outputs)

        self.drop = torch.nn.Dropout(p_dropout)
        self.relu = torch.nn.ReLU()

    def forward(self, x):
        # input layer. All inputs have an effect on all outputs.
        x = self.f1(x)
        x = x / torch.linalg.norm(x)

        return x

