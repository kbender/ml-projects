import math
import pandas as pd
import math
from datetime import datetime, timezone, timedelta


def conv2d_output_dims(w_in, h_in, padding, kernel_size, stride, dilation=1):

    h = (h_in+2*padding-dilation*(kernel_size-1)-1) / stride + 1
    w = (w_in+2*padding-dilation*(kernel_size-1)-1) / stride + 1

    return math.floor(h), math.ceil(w)

def set_up_colab(root_path):
    import os
    from google.colab import drive

    # mount your google drive
    drive.mount('/content/drive')

    # load data;


    from psutil import virtual_memory
    ram_gb = virtual_memory().total / 1e9
    print('Your runtime has {:.1f} gigabytes of available RAM\n'.format(ram_gb))

    if ram_gb < 20:
        print('To enable a high-RAM runtime, select the Runtime > "Change runtime type"')
        print('menu, and then select High-RAM in the Runtime shape dropdown. Then, ')
        print('re-execute this cell.')
    else:
        print('You are using a high-RAM runtime!')

def get_ram():
    from psutil import virtual_memory
    ram_gb = virtual_memory().total / 1e9
    print('Your runtime has {:.1f} gigabytes of available RAM\n'.format(ram_gb))

def save_score(score, time_to_train, networkArchitecture, batch_size, num_epochs, training_size, test_size, losses ,colab, root_path, computerName, image_size, notes):

    data = {'Accuracy': [score],
            'Time to compute': [time_to_train],
            'batch-size': [batch_size],
            'epochs': [num_epochs], 
            'Training data set size': [training_size],
            'Testing data set size': [test_size],
            'Network': [networkArchitecture], 
            'losses':[losses],
            'computer':[computerName],
            'image size':[image_size],
            'notes':[notes]
    }

    df = pd.DataFrame (data, columns = ['Accuracy','Time to compute','batch-size','epochs','Training data set size','Testing data set size','Network','losses', 'computer', 'image size', 'notes'])

    # Uncomment this line and comment the next 2 if you don't have the csv file on 
    # your drive yet. Be careful tough, as this line resets all the information of
    # the file, so don't call it once you already have measurements stored.
    #df.to_csv(root_path + 'assets/measurements.csv', index = False)

    df_old = pd.read_csv(root_path + 'assets/measurements.csv')
    pd.concat([df_old, df]).to_csv(root_path + 'assets/measurements.csv', index = False)

def unique_string():
    tz = timezone(timedelta(hours=2))
    now = datetime.now().astimezone(tz)


    return now.strftime('%d%H%M')